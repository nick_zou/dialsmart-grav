<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://dialsmart/dialsmart.yaml',
    'modified' => 1522352210,
    'data' => [
        'streams' => [
            'schemes' => [
                'theme' => [
                    'type' => 'ReadOnlyStream',
                    'prefixes' => [
                        '' => [
                            0 => 'user/themes/dialsmart',
                            1 => 'user/themes/antimatter'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
